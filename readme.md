Installing
==========

Docker
------
~~~~
# Add Secure APT key
apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
~~~~

~~~~
#Choose *ONE*
# Ubuntu Precise
echo 'deb https://apt.dockerproject.org/repo ubuntu-precise main' > /etc/apt/sources.list.d/docker.list
# Ubuntu Trusty
echo 'deb https://apt.dockerproject.org/repo ubuntu-trusty main' > /etc/apt/sources.list.d/docker.list
# Ubuntu Vivid
echo 'deb https://apt.dockerproject.org/repo ubuntu-vivid main' > /etc/apt/sources.list.d/docker.list
# Debian Jessie
echo 'deb https://apt.dockerproject.org/repo debian-jessie main' > /etc/apt/sources.list.d/docker.list
~~~~

~~~~
# Install
apt-get purge lxc-docker*
apt-get update
apt-get install docker-engine
service docker start
groupadd docker
adduser ${USER} docker
~~~~

Python (Using virtualenvwrapper)
--------------------------------
~~~~
mkvirtualenv docker-assemble
pip install -r requirements.txt
~~~~

Docker Assemble
---------------
~~~~
make install
~~~~

Running
=======
~~~~
docker-assemble <path-to-assemble.yml>
~~~~
